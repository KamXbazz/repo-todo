import { PipeTransform, BadRequestException } from "@nestjs/common";
import { UsuarioRol } from "../usuario-rol.enum";

export class UsuarioRolValitadionPipe implements PipeTransform{
    readonly allowedRols = [
        UsuarioRol.ADMIN,
        UsuarioRol.USUARIO,
    ]

    transform(value: any){
        value = value.toUpperCase();
        if(!this.isStatusValid(value)){
            throw new BadRequestException(`'${value}' rol invalido`);
        }
        return value;
    }

    private isStatusValid(status:any){
        const idx =this.allowedRols.indexOf(status);
        return idx !==-1
    }

}