import { Controller, Post, Body, ValidationPipe, UseGuards, Param, Get } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-crendentials.dto';
import { UsuarioRolValitadionPipe } from './pipes/usuario-rol-validation.pipes';
import { UsuarioRol } from './usuario-rol.enum';
import { GetUsuario } from './get-usuario.decorator';
import { Usuario } from './usuario.entity';
import { AuthGuard } from '@nestjs/passport';
import { AuthCredentialsCreateDto } from './dto/AuthCredentialsCreateDto';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService,
    ){}

    @Get('/pendientes')
    getAllUserPendientes(){
        return this.authService.getAllUserPendientes();
    }

    @Post('/signup')
    signUp(
            @Body(ValidationPipe) authCredentialsDto: AuthCredentialsCreateDto,
            @Body('rol',UsuarioRolValitadionPipe) rol: UsuarioRol
        ):Promise<void>{
        return this.authService.signUp(authCredentialsDto,rol);
    }

    @Post('/signin')
    signIn(@Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto):Promise <{accessToken: string}>{
        return this.authService.signIn(authCredentialsDto);
    }






}
