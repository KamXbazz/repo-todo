import { EntityRepository, Repository } from "typeorm";
import { Usuario } from "./usuario.entity";
import { AuthCredentialsDto } from "./dto/auth-crendentials.dto";
import { UsuarioRol } from "./usuario-rol.enum";
import * as bcrypt  from 'bcrypt';
import { ConflictException, InternalServerErrorException, All } from "@nestjs/common";
import { AuthCredentialsCreateDto } from "./dto/AuthCredentialsCreateDto";
import { TodoEstado } from "src/todo/todo-estado.enum";

@EntityRepository(Usuario)
export class UsuarioRepositorio extends Repository<Usuario>{
    async signUp(authCredentialsCreateDto: AuthCredentialsCreateDto, rol: UsuarioRol): Promise<void>{
        const {nombre,contraseña,correo} =authCredentialsCreateDto;
        const usuario = new Usuario()
        usuario.nombre = nombre;
        usuario.contraseña = contraseña;
        usuario.rol = rol;
        usuario.correo = correo;
        usuario.salt = await bcrypt.genSalt();
        usuario.contraseña = await this.hashContraseña(contraseña,usuario.salt);
        try {
            await usuario.save();
        }catch(error) {
            if(error.code === '23505'){
                throw new ConflictException('Username already exists')
            }else{
                throw new InternalServerErrorException();
            }
        }
    }    
    private async hashContraseña(contraseña: string, salt:string): Promise<string>{
        return bcrypt.hash(contraseña,salt)
    }

    
    async validateUsuarioContraseña(authCredentialsDto: AuthCredentialsDto):Promise<string>{
        const {nombre,contraseña} = authCredentialsDto
        const user = await this.findOne({nombre});

        console.log(nombre)
        if(user && await user.validateContraseña(contraseña)){
            console.log(user)
            return user.nombre  
        }else{
            return null
        }
    }

    async getAllUserPendientes(){
        const query = this.createQueryBuilder('usuario').innerJoinAndSelect("usuario.todos","todo","todo.estado != :estado",{estado: TodoEstado.FINALIZADO});
        const usuariosPendiente = await query.getMany();

        return usuariosPendiente;
    }
}