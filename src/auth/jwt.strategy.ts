import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import {Strategy,ExtractJwt} from 'passport-jwt';
import { InjectRepository } from "@nestjs/typeorm";
import { UsuarioRepositorio } from "./usuario.repository";
import { Usuario } from "./usuario.entity";
import { JwtPayload } from "./jwt-payload.interface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
    constructor(
        @InjectRepository(UsuarioRepositorio)
        private usuarioRepositorio: UsuarioRepositorio,
    ){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: 'secreto1314',
        });
    }

    async validate(payload: JwtPayload): Promise<Usuario>{
        const {nombre} = payload;
        const usuario = await this.usuarioRepositorio.findOne({nombre});
        //console.log(user)
        if(!usuario) {
            throw new UnauthorizedException();
        }
        return usuario; 
    }
}