import { IsString, MinLength, MaxLength, Matches } from 'class-validator';
import { UsuarioRol } from '../usuario-rol.enum';
export class AuthCredentialsCreateDto {
    
    @IsString()
    @MinLength(4)
    @MaxLength(8)
    nombre: string;

    @IsString()
    @Matches(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i, { message: 'Ingrese una direccion de correo valida' })
    correo: string;

    @IsString()
    @MinLength(8)
    @MaxLength(20)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, { message: 'Contraseña debil se recomienda una contraseña alfanumerica con una mayuscula' })
    contraseña: string;

    @IsString()
    rol: UsuarioRol;
}
