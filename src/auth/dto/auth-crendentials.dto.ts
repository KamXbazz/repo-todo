import {IsString, MinLength, Max, MaxLength, Matches} from 'class-validator';
import { UsuarioRol } from '../usuario-rol.enum';

export class AuthCredentialsDto{
    @IsString()
    @MinLength(4)
    @MaxLength(8)
    nombre: string;

    @IsString()
    @MinLength(8)
    @MaxLength(20)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    {message:'Contraseña debil se recomienda una contraseña alfanumerica con una mayuscula'})
    contraseña: string;

}