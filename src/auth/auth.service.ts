import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioRepositorio } from './usuario.repository';
import { JwtService } from '@nestjs/jwt';
import { AuthCredentialsDto } from './dto/auth-crendentials.dto';
import { JwtPayload } from './jwt-payload.interface';
import { AuthCredentialsCreateDto } from './dto/AuthCredentialsCreateDto';
import { UsuarioRol } from './usuario-rol.enum';
import { MailerService } from '@nestjs-modules/mailer';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(UsuarioRepositorio)
        private usuarioRepositorio: UsuarioRepositorio,
        private jwtService: JwtService,
        private readonly mailerService: MailerService,
    ){}

    async signUp(authCredentialsDto: AuthCredentialsCreateDto, rol: UsuarioRol): Promise<void>{
        return this.usuarioRepositorio.signUp(authCredentialsDto,rol);
    }

    async signIn(authCredentialsDto: AuthCredentialsDto ): Promise<{accessToken:string}>{
        const nombre = await this.usuarioRepositorio.validateUsuarioContraseña(authCredentialsDto);

        if(!nombre){
            throw new UnauthorizedException('Credenciales invalidas')
        }

        const payload:JwtPayload = {nombre};

        const accessToken = await this.jwtService.sign(payload);
        return{accessToken}
    }

    async getAllUserPendientes(){
        const usuariosPendiente = await this.usuarioRepositorio.getAllUserPendientes();        
        usuariosPendiente.forEach(u => {
            var message = "Listado de todo pendientes: "
            u.todos.forEach( todo =>{
                    message += todo.descripción;
                    message += ", ";
                
            });
            this.allUsuario(message,u.correo);
        });
    }
    
    @Cron('0 0 8 * * *')
    handleCron(){
        this.getAllUserPendientes();
    }
    

    public allUsuario(message:string,correo: string): void {    
        this
        .mailerService
        .sendMail({
          to: correo, // list of receivers
          from: 'ca.veliz10@gmail.com', // sender address
          subject: 'Todos Pendientes del dia', // Subject line
          text: message, // plaintext body
          html: '<b>welcome</b>', // HTML body content
        })
        .then(() => {
        })
        .catch(() => {});
      }


}
