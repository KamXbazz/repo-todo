import { Entity, Unique, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { UsuarioRol } from "./usuario-rol.enum";
import * as bcrypt from 'bcrypt';
import { Todo } from "src/todo/todo.entity";


@Entity()
@Unique(['nombre','correo'])
export class Usuario extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    nombre: string;

    @Column()
    contraseña: string;

    @Column()
    correo: string;

    @Column()
    rol: UsuarioRol;

    @Column()
    salt: string;

    @OneToMany(type => Todo, todo => todo.usuario, {eager: true})
    todos:Todo[];

    async validateContraseña(contraseña:string):Promise<Boolean>{
        const hash = await bcrypt.hash(contraseña,this.salt);
        return hash === this.contraseña;
    }

}