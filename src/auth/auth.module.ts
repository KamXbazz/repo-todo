import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {JwtModule } from '@nestjs/jwt';
import {PassportModule} from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { UsuarioRepositorio } from './usuario.repository';

@Module({
  imports: [PassportModule.register({defaultStrategy: 'jwt'}),
  JwtModule.register({
    secret: 'secreto1314',
    signOptions: {
      expiresIn: 3600,
    },
  }),
  TypeOrmModule.forFeature([UsuarioRepositorio]),

],
  controllers: [AuthController],
  providers: [
    AuthService,  
    JwtStrategy,
  ],
  exports: [
    JwtStrategy,
    PassportModule,
  ],
})
export class AuthModule {}
