import { Module } from '@nestjs/common';
import { TodoController } from './todo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TodoService } from './todo.service';
import { TodoRepository } from './todo.repository';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([TodoRepository]),
    AuthModule,
  ],
  controllers: [TodoController],
  providers: [TodoService]
})
export class TodoModule {}
