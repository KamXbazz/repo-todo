import { Injectable, NotFoundException, RequestTimeoutException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TodoRepository } from './todo.repository';
import { GetTodoFilterDto } from './dto/get-todo-filter.dto';
import { Todo } from './todo.entity';
import { TodoEstado } from './todo-estado.enum';
import { CreateTodoDto } from './dto/create-todo.dto';
import { Usuario } from 'src/auth/usuario.entity';

@Injectable()
export class TodoService {
    constructor(
        @InjectRepository(TodoRepository)
        private todoRepository: TodoRepository,
    ){}

    async createTodo(
        createTodo:CreateTodoDto,
        usuario : Usuario,
        ): Promise<Todo>{
        return this.todoRepository.createTodo(createTodo,usuario);
    }

    async getTodos(
        filterDto: GetTodoFilterDto,
        usuario: Usuario,):Promise<Todo[]>{
        return this.todoRepository.getTodos(filterDto,usuario);
    }


    async getTodoById(id:number, usuario: Usuario):Promise<Todo>{
        const found = await this.todoRepository.findOne({where: {id,usuarioId: usuario.id}});
        if(!found){
            throw new NotFoundException(`El todo con ID '${id}' no fue encontrado`)
        }
        return found;
    }

    async deleteTask(id:number,usuario:Usuario): Promise<void>{
       const result = await this.todoRepository.delete({id, usuarioId: usuario.id});

       if(result.affected ===0){
           throw new NotFoundException(`Todo con el ID '${id}' no ha sido encontrado`)
       }
    }

    async updateTodoStatus(
        id:number,
        estado: TodoEstado, 
        usuario: Usuario,
        ): Promise<Todo>{
        const todo = await this.getTodoById(id,usuario);
        console.log('estado del todo antes de update',todo.estado)
        if(todo.estado != TodoEstado.FINALIZADO) {            
            todo.estado = estado
            if(estado === TodoEstado.FINALIZADO){
                const finish = new Date();
                todo.hora_finalización = finish.getHours() + ':' +finish.getMinutes();
                todo.fecha_finalización = finish.getFullYear()+"-"+(finish.getMonth() +1)+"-"+ finish.getDate()
            }
            await todo.save();
            return todo;
        }
    }






}
