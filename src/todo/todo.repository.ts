import { Entity, EntityRepository, Repository, Timestamp } from "typeorm";
import { Todo } from "./todo.entity";
import { CreateTodoDto } from "./dto/create-todo.dto";
import { TodoEstado } from "./todo-estado.enum";
import { GetTodoFilterDto } from "./dto/get-todo-filter.dto";
import { Usuario } from "src/auth/usuario.entity";
import { UsuarioRol } from "src/auth/usuario-rol.enum";

@EntityRepository(Todo)
export class TodoRepository extends Repository<Todo>{
    async createTodo(
        createTodoDto: CreateTodoDto,
        usuario: Usuario,
        ): Promise<Todo>{
        console.log(usuario)
        const {descripción} = createTodoDto;
        const todo = new Todo();
        todo.descripción = descripción;
        todo.estado = TodoEstado.INICIADO;
        todo.usuario = usuario;
        await todo.save();
        
        delete todo.usuario;
        
        return todo;
    }

    async getTodos(
        filterDto:GetTodoFilterDto,
        usuario: Usuario
        ): Promise<Todo[]>{
        const {estado,search} = filterDto;
        const query = this.createQueryBuilder('todo');
        if(usuario.rol !== UsuarioRol.ADMIN){          
            query.where('todo.usuarioId = :usuarioId',{usuarioId: usuario.id})
        }
        
        if(estado){
            query.andWhere('todo.estado = :estado', {estado});
        }
        if(search) {
            query.andWhere('(todo.fecha_creación >= :search OR todo.fecha_finalización <= :search)',{search})
        }
        const todos = await query.getMany();
        return todos;
    }

    async getTodosPendientes(
    ): Promise<Todo[]>{
        const query = this.createQueryBuilder('todo');
        query.andWhere('todo.estado != :estado',{estado: TodoEstado.FINALIZADO});
        const todos = await query.getMany();
        return todos;
    }



}