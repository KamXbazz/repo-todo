export enum TodoEstado {
    INICIADO = 'INICIADO',
    PENDIENTE = 'PENDIENTE',
    FINALIZADO = 'FINALIZADO',
}