import { PipeTransform, BadRequestException } from "@nestjs/common";
import { TodoEstado } from "../todo-estado.enum";

export class TodoEstadoValidation implements PipeTransform{
    readonly estadosPermitidos = [
        TodoEstado.INICIADO,
        TodoEstado.PENDIENTE,
        TodoEstado.FINALIZADO
    ];

    transform(value: any){
        value = value.toUpperCase();
        if(!this.isStatusValid(value)){
            throw new BadRequestException(`'${value}' es un estado invalido`)
        }
        return value
    }

    private isStatusValid(status: any){
        const idx = this.estadosPermitidos.indexOf(status);
        return idx !== -1;
    }


}