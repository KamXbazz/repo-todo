import { Controller, UseGuards, Get, Query, ValidationPipe, Param, ParseIntPipe, Post, UsePipes, Body, Delete, Patch } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TodoService } from './todo.service';
import { GetTodoFilterDto } from './dto/get-todo-filter.dto';
import { Todo } from './todo.entity';
import { CreateTodoDto } from './dto/create-todo.dto';
import { TodoEstadoValidation } from './pipes/todo-estado-validation.pipe';
import { TodoEstado } from './todo-estado.enum';
import { Usuario } from 'src/auth/usuario.entity';
import { GetUsuario } from 'src/auth/get-usuario.decorator';

@Controller('todo')
@UseGuards(AuthGuard())
export class TodoController {
    constructor(private todoService: TodoService){}
  
    @Get()
    getTodos(
        @Query(ValidationPipe) filterDto:GetTodoFilterDto,
        @GetUsuario() usuario: Usuario,
        ){
        return this.todoService.getTodos(filterDto,usuario);
    }

    @Get('/:id')
    getTodoById(@Param('id',ParseIntPipe) id: number,
    @GetUsuario() usuario: Usuario):Promise<Todo>{
        return this.todoService.getTodoById(id,usuario);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createTodo(
        @Body() createTodoDto: CreateTodoDto,
        @GetUsuario() usuario : Usuario,
    ): Promise<Todo>{
        return this.todoService.createTodo(createTodoDto, usuario);
    }

    @Delete('/:id')
    deleteTodo(
        @Param('id',ParseIntPipe) id: number,
        @GetUsuario() usuario:Usuario,
    ):Promise<void>{
        return this.todoService.deleteTask(id,usuario);
    }
    
    @Patch('/:id/estado')
    updateTodoEstado(
        @Param('id', ParseIntPipe) id: number,
        @Body('estado', TodoEstadoValidation) estado: TodoEstado,
        @GetUsuario() usuario: Usuario,
    ):Promise<Todo>{
        console.log(estado)
        return this.todoService.updateTodoStatus(id,estado,usuario)
    }


}
