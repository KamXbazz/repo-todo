import { IsOptional, IsIn } from "class-validator";
import { TodoEstado } from "../todo-estado.enum";

export class GetTodoFilterDto {
    @IsOptional()
    @IsIn([TodoEstado.FINALIZADO,TodoEstado.INICIADO,TodoEstado.PENDIENTE])
    estado: TodoEstado;

    @IsOptional()
    search: Date

}