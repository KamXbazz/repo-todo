import {IsNotEmpty, IsOptional} from 'class-validator';

export class CreateTodoDto {
    @IsNotEmpty()
    descripción: string;
}