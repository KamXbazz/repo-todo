import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, Timestamp, ManyToOne } from "typeorm";
import { TodoEstado } from "./todo-estado.enum";
import { Usuario } from "src/auth/usuario.entity";

@Entity()
export class Todo extends BaseEntity{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    descripción: string;
    
    @Column({ type: "date", default: () => "CURRENT_DATE"})
    fecha_creación: string;

    @Column({type: "time" , default : () =>"CURRENT_TIME"})
    hora_creación: string;
    
    @Column({ type: 'date', nullable: true, default: null })
    fecha_finalización: string;

    @Column({type: "time" , default : null})
    hora_finalización: string;
    
    @Column()
    estado: TodoEstado

    @ManyToOne(type => Usuario, usuario=> usuario.todos, {eager: false})
    usuario: Usuario;

    @Column()
    usuarioId: number;
}