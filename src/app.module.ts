import { Module } from '@nestjs/common';
import { PugAdapter, MailerModule } from '@nestjs-modules/mailer';
import { TodoModule } from './todo/todo.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    TodoModule,
    AuthModule,  
    MailerModule.forRoot({
      transport: {
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
        user: "c959b56464dcca",
        pass: "9179ab1558fb8b"
        } 
      },
      preview: false,
      template: {
        dir: process.cwd() + '/template/',
        adapter: new PugAdapter(), // or new PugAdapter() or new EjsAdapter()
        options: {
          strict: true,
        },
      },
    }),
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
